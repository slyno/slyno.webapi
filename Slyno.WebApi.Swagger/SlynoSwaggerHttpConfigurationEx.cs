﻿using System;
using System.Web.Http;
using Swashbuckle.Application;

namespace Slyno.WebApi.Swagger
{
    public static class SlynoSwaggerHttpConfigurationEx
    {
        /// <summary>
        /// Enables the basic setup of Swagger with the Slyno defaults in place.
        /// </summary>
        public static void SlynoEnableSwagger(this HttpConfiguration config, string version = "v1", string title = "Slyno WebApi Application")
        {
            if (string.IsNullOrWhiteSpace(version))
            {
                throw new ArgumentNullException(nameof(version), "Version must not be null.");
            }

            if (string.IsNullOrWhiteSpace(title))
            {
                throw new ArgumentNullException(nameof(title), "Title must not be null.");
            }

            config.EnableSwagger(c =>
            {
                c.SingleApiVersion(version.Replace(".", "-"), title);
                c.OperationFilter(() => new AuthorizationOperationFilter());
            })
            .EnableSwaggerUi(c =>
            {
                c.DisableValidator();
            });
        }
    }
}
