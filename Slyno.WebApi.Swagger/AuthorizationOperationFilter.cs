﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using Swashbuckle.Swagger;

namespace Slyno.WebApi.Swagger
{
    /// <summary>
    /// This will add an input for putting the Authorization header.
    /// Any route that uses the AllowAnonymous attribute will not have this added to Swagger.
    /// This is pre-filled with Bearer.
    /// </summary>
    public class AuthorizationOperationFilter : IOperationFilter
    {
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            var actionDescription = apiDescription.ActionDescriptor;

            //check the method and the class
            var allowAnonymous = actionDescription.GetCustomAttributes<AllowAnonymousAttribute>().Any() ||
                actionDescription.ControllerDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any();

            if (allowAnonymous)
            {
                return;
            }

            if (operation.parameters == null)
            {
                operation.parameters = new List<Parameter>();
            }

            operation.parameters.Add(new Parameter
            {
                name = "Authorization",
                @in = "header",
                description = "Bearer AccessToken",
                type = "string",
                @default = "Bearer "
            });
        }
    }
}
