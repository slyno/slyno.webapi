﻿using System;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using Microsoft.Practices.Unity;
using Slyno.WebApi.Auth;
using Slyno.WebApi.Data.Crud;
using Slyno.WebApi.Data.Rules;

namespace Slyno.WebApi.Unity
{
    public static class SlynoUnityHttpConfigurationEx
    {
        /// <summary>
        /// Sets up Unity as the IoC container. Default services for SlynoWebApi are added automatically.
        /// </summary>
        /// <param name="config"></param>
        /// <param name="assembly">This assembly will be scanned for Rules to be added to the container. Typically pass Assembly.GetExecutingAssembly().</param>
        /// <param name="callback">This method will be called with the container so additional services can be added.</param>
        public static void SlynoEnableUnity(this HttpConfiguration config, Assembly assembly, Action<IUnityContainer> callback)
        {
            var container = new UnityContainer();

            var rulesService = new RulesService(container, assembly);

            container.RegisterInstance(typeof(IRulesService), rulesService, new ContainerControlledLifetimeManager());

            callback(container);

            var registrations = container.Registrations.ToList();

            if (registrations.All(x => x.RegisteredType != typeof(IPatcher)))
            {
                container.RegisterType<IPatcher, Patcher>(new ContainerControlledLifetimeManager());
            }

            if (registrations.All(x => x.RegisteredType != typeof(ISecureStringService)))
            {
                container.RegisterType<ISecureStringService, SecureStringService>(new ContainerControlledLifetimeManager());
            }

            config.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}
