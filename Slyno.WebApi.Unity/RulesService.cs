﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Practices.Unity;
using Slyno.WebApi.Data.Models;
using Slyno.WebApi.Data.Rules;

namespace Slyno.WebApi.Unity
{
    sealed class RulesService : IRulesService
    {
        private readonly UnityContainer _container;

        public RulesService(UnityContainer container, Assembly assembly)
        {
            _container = container;

            var ruleFinder = new RuleFinder(assembly);

            var createRules = ruleFinder.FindCreateRules();
            var updateRules = ruleFinder.FindUpdateRules();
            var deleteRules = ruleFinder.FindDeleteRules();

            foreach (var rule in createRules)
            {
                container.RegisterType(typeof(ICreateRule<>), rule, rule.FullName);
            }

            foreach (var rule in updateRules)
            {
                container.RegisterType(typeof(IUpdateRule<>), rule, rule.FullName);
            }

            foreach (var rule in deleteRules)
            {
                container.RegisterType(typeof(IDeleteRule<>), rule, rule.FullName);
            }
        }

        public IEnumerable<ICreateRule<T>> GetCreateRules<T>(RuleType type) where T : IModel
        {
            return _container.ResolveAll<ICreateRule<T>>()
                .Where(x => Attribute.IsDefined(x.GetType(), RuleAttribute.GetRuleAttributeType(type), true));
        }

        public IEnumerable<IUpdateRule<T>> GetUpdateRules<T>(RuleType type) where T : IModel
        {
            return _container.ResolveAll<IUpdateRule<T>>()
                .Where(x => Attribute.IsDefined(x.GetType(), RuleAttribute.GetRuleAttributeType(type), true));
        }

        public IEnumerable<IDeleteRule<T>> GetDeleteRules<T>(RuleType type) where T : IModel
        {
            return _container.ResolveAll<IDeleteRule<T>>()
                .Where(x => Attribute.IsDefined(x.GetType(), RuleAttribute.GetRuleAttributeType(type), true));
        }
    }
}
