﻿using System;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using Slyno.WebApi.Auth;
using Slyno.WebApi.Data.Crud;
using Slyno.WebApi.Data.Rules;

namespace Slyno.WebApi.SimpleInjector
{
    public static class SlynoSimpleInjectorHttpConfigurationEx
    {
        /// <summary>
        /// Sets up SimpleInjector as the IoC container. Default services for SlynoWebApi are added automatically.
        /// </summary>
        /// <param name="config"></param>
        /// <param name="assembly">This assembly will be scanned for Rules to be added to the container. Typically pass Assembly.GetExecutingAssembly().</param>
        /// <param name="callback">This method will be called with the container so additional services can be added.</param>
        public static void SlynoEnableSimpleInjector(this HttpConfiguration config, Assembly assembly, Action<Container> callback)
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebApiRequestLifestyle();
            container.RegisterWebApiControllers(config);

            var rulesService = new RulesService(container, assembly);

            container.RegisterSingleton<IRulesService>(rulesService);

            callback(container);

            var registrations = container.GetCurrentRegistrations().ToList();

            if (registrations.All(x => x.ServiceType != typeof(IPatcher)))
            {
                container.RegisterSingleton<IPatcher, Patcher>();
            }

            if (registrations.All(x => x.ServiceType != typeof(ISecureStringService)))
            {
                container.RegisterSingleton<ISecureStringService, SecureStringService>();
            }

            container.Verify();

            config.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
        }
    }
}
