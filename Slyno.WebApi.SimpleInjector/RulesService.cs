﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using SimpleInjector;
using Slyno.WebApi.Data.Models;
using Slyno.WebApi.Data.Rules;

namespace Slyno.WebApi.SimpleInjector
{
    sealed class RulesService : IRulesService
    {
        private readonly Container _container;

        public RulesService(Container container, Assembly assembly)
        {
            _container = container;

            var ruleFinder = new RuleFinder(assembly);
            container.RegisterCollection(typeof(ICreateRule<>), ruleFinder.FindCreateRules());
            container.RegisterCollection(typeof(IUpdateRule<>), ruleFinder.FindUpdateRules());
            container.RegisterCollection(typeof(IDeleteRule<>), ruleFinder.FindDeleteRules());
        }

        public IEnumerable<ICreateRule<T>> GetCreateRules<T>(RuleType type) where T : IModel
        {
            return _container.GetAllInstances<ICreateRule<T>>()
                .Where(x => Attribute.IsDefined(x.GetType(), RuleAttribute.GetRuleAttributeType(type), true));
        }

        public IEnumerable<IUpdateRule<T>> GetUpdateRules<T>(RuleType type) where T : IModel
        {
            return _container.GetAllInstances<IUpdateRule<T>>()
                .Where(x => Attribute.IsDefined(x.GetType(), RuleAttribute.GetRuleAttributeType(type), true));
        }

        public IEnumerable<IDeleteRule<T>> GetDeleteRules<T>(RuleType type) where T : IModel
        {
            return _container.GetAllInstances<IDeleteRule<T>>()
                .Where(x => Attribute.IsDefined(x.GetType(), RuleAttribute.GetRuleAttributeType(type), true));
        }
    }
}
