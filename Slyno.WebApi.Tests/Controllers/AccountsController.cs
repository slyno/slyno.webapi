﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Newtonsoft.Json.Linq;
using Slyno.WebApi.Controllers;
using Slyno.WebApi.Data.Crud;
using Slyno.WebApi.Extensions;
using Slyno.WebApi.Loggers;
using Slyno.WebApi.Tests.Dtos;
using Slyno.WebApi.Tests.Models;

namespace Slyno.WebApi.Tests.Controllers
{
    public class AccountsController : ModelApiController<Account>
    {
        private readonly ILogger _logger;

        public AccountsController(ILogger logger, ICrudService crudService) : base(crudService)
        {
            _logger = logger;
        }

        [Route("Accounts")]
        public IEnumerable<Account> Get()
        {
            _logger.Log("GET collection called.");

            return this.CrudService.ReadCollection<Account>().ToList();
        }

        private const string GetAccountRoute = nameof(GetAccountRoute);
        [AllowAnonymous]
        [Route("Accounts/{id:int}", Name = GetAccountRoute)]
        public async Task<Account> Get(int id)
        {
            _logger.Log("GET called.");

            return await this.CrudService.ReadSingleAsync<Account>(id);
        }

        [Route("Accounts")]
        [HttpPost]
        [ResponseType(typeof(Account))]
        public async Task<IHttpActionResult> Post(AccountDtoIn dtoIn)
        {
            _logger.Log("POST called.");

            var account = new Account
            {
                Title = dtoIn.Title,
            };

            await this.CreateAsync(account);

            return this.CreatedAtRoute(GetAccountRoute, new { id = account.Id }, account);
        }

        [Route("Accounts/{id:int}")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int id)
        {
            _logger.Log("DELETE called.");

            var item = await this.CrudService.ReadSingleAsync<Account>(id);

            await this.DeleteAsync(item);

            return this.OkNoContent();
        }

        [Route("Accounts/{id:int}")]
        [HttpPatch]
        public async Task<Account> Patch(int id, JObject delta)
        {
            _logger.Log("PATCH called.");

            var item = await this.CrudService.ReadSingleAsync<Account>(id);

            await this.UpdateAsync(item, delta);

            return item;
        }
    }
}
