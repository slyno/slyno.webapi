﻿using System.Web.Http;
using Slyno.WebApi.Loggers;

namespace Slyno.WebApi.Tests.Controllers
{
    public class TestController : ApiController
    {
        private readonly ILogger _logger;

        public TestController(ILogger logger)
        {
            _logger = logger;
        }

        [Route("Test")]
        public string Get()
        {
            _logger.Log("Get called on TestController.");
            return "Test";
        }
    }
}
