﻿using System.Reflection;
using System.Web.Http;
using Microsoft.Practices.Unity;
using Slyno.WebApi.Auth;
using Slyno.WebApi.Data.Crud;
using Slyno.WebApi.Loggers;
using Slyno.WebApi.SimpleInjector;
using Slyno.WebApi.Swagger;
using Slyno.WebApi.Unity;

namespace Slyno.WebApi.Tests
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.SlynoWebApi();

            //config.SlynoEnableSimpleInjector(Assembly.GetExecutingAssembly(), container =>
            //{
            //    container.RegisterSingleton<ILogger, DebugLogger>();
            //    container.RegisterSingleton<ICrudService, FakeCrudService>();
            //});

            config.SlynoEnableUnity(Assembly.GetExecutingAssembly(), container =>
            {
                container.RegisterType<ILogger, DebugLogger>(new ContainerControlledLifetimeManager());
                container.RegisterType<ICrudService, FakeCrudService>(new HierarchicalLifetimeManager());
                container.RegisterType<IApiPrincipalProvider, ApiPrincipalProvider>(new HierarchicalLifetimeManager());
            });

            config.SlynoEnableSwagger();
        }
    }
}
