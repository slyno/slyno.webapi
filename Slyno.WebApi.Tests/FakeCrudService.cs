﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Slyno.WebApi.Auth;
using Slyno.WebApi.Data.Crud;
using Slyno.WebApi.Data.Models;
using Slyno.WebApi.Data.Rules;
using Slyno.WebApi.Exceptions;
using Slyno.WebApi.Tests.Models;

namespace Slyno.WebApi.Tests
{
    sealed class FakeCrudService : CrudService
    {
        private readonly ISecureStringService _secureStringService;

        private readonly Dictionary<Type, List<IModel>> _tables = new Dictionary<Type, List<IModel>>();

        public FakeCrudService(IPatcher patcher, IRulesService rulesService, ISecureStringService secureStringService) : base(patcher, rulesService)
        {
            _secureStringService = secureStringService;

            this.Seed();
        }

        private void Seed()
        {
            var accounts = new List<IModel>();
            for (var i = 1; i <= 100; i++)
            {
                accounts.Add(new Account
                {
                    Id = i,
                    Token = _secureStringService.GenerateRandomString(10)
                });
            }

            _tables.Add(typeof(Account), accounts);
        }

        public override Task<T> ReadSingleOrDefaultAsync<T>(int id)
        {
            var model = this.ReadCollection<T>().FirstOrDefault(x => x.Id == id);
            return Task.FromResult(model);
        }

        public override Task<T> ReadSingleOrDefaultAsync<T>(Guid uniqueId)
        {
            var model = this.ReadCollection<T>().FirstOrDefault(x => x.UniqueId == uniqueId);
            return Task.FromResult(model);
        }

        public override Task<T> ReadSingleOrDefaultAsync<T>(string token)
        {
            var model = this.ReadCollection<T>().FirstOrDefault(x => x.Token == token);

            //ensures a case sensitive token
            return model == null ? Task.FromResult<T>(null) :
                Task.FromResult(string.Equals(model.Token, token) ? model : null);
        }

        public override IQueryable<T> ReadCollection<T>()
        {
            return this.GetTable<T>().OfType<T>().AsQueryable();
        }

        protected override Task PersistCreateAsync<T>(T model)
        {
            var table = this.GetTable<T>();

            var newId = table.Max(x => x.Id) + 1;

            model.Id = newId;

            table.Add(model);

            return Task.CompletedTask;
        }

        protected override Task PersistUpdateAsync<T>(T model)
        {
            //nothing to do since this is in memory and the model is already updated.
            return Task.CompletedTask;
        }

        protected override Task PersistDeleteAsync<T>(T model)
        {
            this.GetTable<T>().Remove(model);

            return Task.CompletedTask;
        }

        private List<IModel> GetTable<T>() where T : class, IModel
        {
            var type = typeof(T);

            if (!_tables.ContainsKey(type))
            {
                throw new NotFoundException();
            }

            var list = _tables[type];
            if (list == null)
            {
                throw new NotFoundException();
            }

            return list;
        }
    }
}
