﻿using System;
using System.Threading.Tasks;
using Slyno.WebApi.Auth;
using Slyno.WebApi.Loggers;
using Slyno.WebApi.Tests.Models;

namespace Slyno.WebApi.Tests
{
    public class ApiPrincipalProvider : IApiPrincipalProvider
    {
        public ApiPrincipalProvider(ILogger logger)
        {
            logger.Log("New ApiPrincipalProvider Instance");
        }

        public Task<IApiPrincipal> GetApiPrincipalAsync(string token)
        {
            if (token != "a")
            {
                return Task.FromResult<IApiPrincipal>(null);
            }

            var principal = new ApiPrincipal(token, new User(), new Application(), DateTime.UtcNow.AddHours(1));

            return Task.FromResult<IApiPrincipal>(principal);
        }
    }
}
