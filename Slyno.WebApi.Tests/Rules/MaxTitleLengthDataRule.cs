﻿using System.Threading.Tasks;
using Slyno.WebApi.Auth;
using Slyno.WebApi.Data.Crud;
using Slyno.WebApi.Data.Rules;
using Slyno.WebApi.Exceptions;
using Slyno.WebApi.Loggers;
using Slyno.WebApi.Tests.Models;

namespace Slyno.WebApi.Tests.Rules
{
    [DataRule]
    sealed class MaxTitleLengthDataRule : ICreateRule<Account>, IUpdateRule<Account>
    {
        private readonly ILogger _logger;

        public MaxTitleLengthDataRule(ILogger logger)
        {
            _logger = logger;
        }

        public Task ExecuteAsync(Account model, ICrudService crudService, IApiPrincipal principal)
        {
            this.VerifyMaxLength(model.Title);

            return Task.CompletedTask;
        }

        public Task ExecuteAsync(Account model, PatchResult patchResult, ICrudService crudService, IApiPrincipal principal)
        {
            if (!patchResult.NewValues.ContainsKey(nameof(model.Title)))
            {
                return Task.CompletedTask;
            }

            var title = patchResult.NewValues[nameof(model.Title)].ToString();

            this.VerifyMaxLength(title);

            return Task.CompletedTask;
        }

        private void VerifyMaxLength(string title)
        {
            _logger.Log($"{this.GetType().Name} executing...");

            if (string.IsNullOrWhiteSpace(title))
            {
                throw new BadRequestException("Title must not be null or whitespace.");
            }

            if (title.Length > 10)
            {
                throw new OutOfRangeException("Title must not be more than 10 characters.");
            }
        }
    }
}
