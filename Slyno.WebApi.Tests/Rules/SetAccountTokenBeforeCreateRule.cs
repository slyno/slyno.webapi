﻿using System.Threading.Tasks;
using Slyno.WebApi.Auth;
using Slyno.WebApi.Data.Crud;
using Slyno.WebApi.Data.Rules;
using Slyno.WebApi.Loggers;
using Slyno.WebApi.Tests.Models;

namespace Slyno.WebApi.Tests.Rules
{
    [BeforeCrudRule]
    sealed class SetAccountTokenBeforeCreateRule : ICreateRule<Account>
    {
        private readonly ILogger _logger;
        private readonly ISecureStringService _secureStringService;

        public SetAccountTokenBeforeCreateRule(ILogger logger, ISecureStringService secureStringService)
        {
            _logger = logger;
            _secureStringService = secureStringService;
        }

        public Task ExecuteAsync(Account model, ICrudService crudService, IApiPrincipal principal)
        {
            _logger.Log($"{this.GetType().Name} executing...");

            model.Token = _secureStringService.GenerateRandomString(10);

            return Task.CompletedTask;
        }
    }
}
