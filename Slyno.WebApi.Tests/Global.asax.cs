﻿using System.Web;
using System.Web.Http;

namespace Slyno.WebApi.Tests
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
