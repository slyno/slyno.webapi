﻿using System.Diagnostics;
using Slyno.WebApi.Loggers;

namespace Slyno.WebApi.Tests
{
    public class DebugLogger : ILogger
    {
        public void Log(string message)
        {
            Debug.WriteLine(message);
        }
    }
}
