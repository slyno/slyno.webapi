﻿using System;
using Slyno.WebApi.Data.Models;

namespace Slyno.WebApi.Tests.Models
{
    public sealed class Account : IModel, IUnique, ITokenizable
    {
        public Account()
        {
            this.UniqueId = Guid.NewGuid();
        }

        public int Id { get; set; }

        public Guid UniqueId { get; set; }

        public string Token { get; set; }

        public string Title { get; set; }
    }
}
