﻿using System;
using Slyno.WebApi.Auth;

namespace Slyno.WebApi.Tests.Models
{
    sealed class User : IUser
    {
        public int Id { get; set; } = 1;

        public Guid UniqueId { get; set; } = Guid.NewGuid();

        public string FirstName { get; set; } = "Test";

        public string LastName { get; set; } = "User";

        public string Email { get; set; } = "test@user.com";
    }
}
