﻿using System;
using Slyno.WebApi.Auth;

namespace Slyno.WebApi.Tests.Models
{
    sealed class Application : IApplication
    {
        public int Id { get; set; } = 1;

        public Guid UniqueId { get; set; } = Guid.NewGuid();

        public string Name { get; set; } = "Test Application";

        public string Version { get; set; } = "0.1";
    }
}
