nuget.exe pack Slyno.WebApi\Slyno.WebApi.csproj -Prop Configuration=Release
nuget.exe pack Slyno.WebApi.SimpleInjector\Slyno.WebApi.SimpleInjector.csproj -Prop Configuration=Release
nuget.exe pack Slyno.WebApi.Swagger\Slyno.WebApi.Swagger.csproj -Prop Configuration=Release
nuget.exe pack Slyno.WebApi.Unity\Slyno.WebApi.Unity.csproj -Prop Configuration=Release
nuget.exe push * -Source https://www.nuget.org/api/v2/package
del *.nupkg
pause
