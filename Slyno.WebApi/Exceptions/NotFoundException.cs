﻿using System.Net;

namespace Slyno.WebApi.Exceptions
{
    public class NotFoundException : ApiException
    {
        public NotFoundException() : this("Not Found")
        {
        }

        public NotFoundException(string message) : base(message, HttpStatusCode.NotFound)
        {
        }
    }
}
