﻿using System.Net;

namespace Slyno.WebApi.Exceptions
{
    public class ConflictException : ApiException
    {
        public ConflictException() : this("Conflict")
        {
        }

        public ConflictException(string message) : base(message, HttpStatusCode.Conflict)
        {
        }
    }
}
