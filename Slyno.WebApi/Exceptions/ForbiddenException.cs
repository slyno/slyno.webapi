﻿using System.Net;

namespace Slyno.WebApi.Exceptions
{
    public class ForbiddenException : ApiException
    {
        public ForbiddenException() : this("Forbidden")
        {
        }

        public ForbiddenException(string message) : base(message, HttpStatusCode.Forbidden)
        {
        }
    }
}
