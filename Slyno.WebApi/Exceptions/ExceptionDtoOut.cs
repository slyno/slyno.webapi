﻿using System;

namespace Slyno.WebApi.Exceptions
{
    public class ExceptionDtoOut
    {
        public ExceptionDtoOut(string message, Guid correlationId)
        {
            this.Message = message;
            this.CorrelationId = correlationId;
        }

        public string Message { get; }

        public Guid CorrelationId { get; }
    }
}
