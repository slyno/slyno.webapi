﻿using System.Net;

namespace Slyno.WebApi.Exceptions
{
    public class UnsupportedMediaTypeException : ApiException
    {
        public UnsupportedMediaTypeException() : this("Unsupported MediaType")
        {
        }

        public UnsupportedMediaTypeException(string message) : base(message, HttpStatusCode.UnsupportedMediaType)
        {
        }
    }
}
