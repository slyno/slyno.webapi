﻿using System.Net;

namespace Slyno.WebApi.Exceptions
{
    public class ServiceUnavailableException : ApiException
    {
        public ServiceUnavailableException() : this("Service Unavailable")
        {
        }

        public ServiceUnavailableException(string message) : base(message, HttpStatusCode.ServiceUnavailable)
        {
        }
    }
}
