﻿using System;
using System.Net;

namespace Slyno.WebApi.Exceptions
{
    public class ApiException : Exception
    {
        public ApiException() : this("Bad Request")
        {
        }

        public ApiException(HttpStatusCode statusCode)
        {
            this.HttpStatusCode = statusCode;
        }

        public ApiException(string message) : base(message)
        {
        }

        public ApiException(string message, HttpStatusCode statusCode) : base(message)
        {
            this.HttpStatusCode = statusCode;
        }

        public HttpStatusCode HttpStatusCode { get; } = HttpStatusCode.BadRequest;
    }
}
