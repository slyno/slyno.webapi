﻿using System.Net;

namespace Slyno.WebApi.Exceptions
{
    public class UnauthorizedException : ApiException
    {
        public UnauthorizedException() : this("Unauthorized")
        {
        }

        public UnauthorizedException(string message) : base(message, HttpStatusCode.Unauthorized)
        {
        }
    }
}
