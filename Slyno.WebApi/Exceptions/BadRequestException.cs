﻿namespace Slyno.WebApi.Exceptions
{
    public class BadRequestException : ApiException
    {
        public BadRequestException()
        {
        }

        public BadRequestException(string message) : base(message)
        {
        }
    }
}
