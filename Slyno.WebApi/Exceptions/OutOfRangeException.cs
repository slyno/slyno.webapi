﻿namespace Slyno.WebApi.Exceptions
{
    public class OutOfRangeException : ApiException
    {
        public OutOfRangeException(string message) : base(message)
        {
        }
    }
}
