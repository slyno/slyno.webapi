﻿namespace Slyno.WebApi.Exceptions
{
    public class PasswordValidationException : ApiException
    {
        public PasswordValidationException(string message) : base(message)
        {
        }
    }
}
