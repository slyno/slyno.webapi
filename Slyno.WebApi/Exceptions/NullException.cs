﻿namespace Slyno.WebApi.Exceptions
{
    public class NullException : ApiException
    {
        public NullException(string message) : base(message)
        {
        }
    }
}
