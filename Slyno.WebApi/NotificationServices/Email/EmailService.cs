﻿using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Slyno.WebApi.NotificationServices.Email
{
    public abstract class EmailService : IEmailService
    {
        private const string EmailRegex = @"^[a-zA-Z0-9_\.\-\+]+@[a-zA-Z0-9_\.\-]+\.[a-zA-Z]{2,4}$";

        public abstract Task SendAsync(IEmailMessage emailMessage);

        public bool IsValidEmailAddress(string emailAddress)
        {
            return !string.IsNullOrWhiteSpace(emailAddress) && Regex.IsMatch(emailAddress, EmailRegex);
        }
    }
}
