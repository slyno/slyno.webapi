﻿using System.Collections.Generic;

namespace Slyno.WebApi.NotificationServices.Email
{
    public interface IEmailMessage
    {
        string FromAddress { get; }

        string ReplyToAddress { get; }

        IEnumerable<string> ToAddresses { get; }

        IEnumerable<string> CcAddresses { get; }

        IEnumerable<string> BccAddresses { get; }

        string Subject { get; }

        string BodyPlainText { get; }

        string BodyHtml { get; }
    }
}
