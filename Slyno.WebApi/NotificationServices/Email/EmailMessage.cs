﻿using System.Collections.Generic;

namespace Slyno.WebApi.NotificationServices.Email
{
    public class EmailMessage : IEmailMessage
    {
        public string FromAddress { get; set; }

        public string ReplyToAddress { get; set; }

        public IEnumerable<string> ToAddresses { get; set; } = new List<string>();

        public IEnumerable<string> CcAddresses { get; set; } = new List<string>();

        public IEnumerable<string> BccAddresses { get; set; } = new List<string>();

        public string Subject { get; set; }

        public string BodyPlainText { get; set; }

        public string BodyHtml { get; set; }
    }
}
