﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Slyno.WebApi.NotificationServices.Email
{
    public class SendGridEmailService : EmailService
    {
        private const string SendEnpoint = "https://api.sendgrid.com/v3/mail/send";
        private readonly string _apiKey;

        public SendGridEmailService(string apiKey)
        {
            _apiKey = apiKey;
        }

        public override async Task SendAsync(IEmailMessage emailMessage)
        {
            this.Validate(emailMessage);

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _apiKey);

                var response = await client.PostAsync(SendEnpoint, GetHttpContent(emailMessage));

                if (!response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync();
                    throw new Exception($"Error sending email: {data}");
                }
            }
        }

        private void Validate(IEmailMessage emailMessage)
        {
            if (this.IsValidEmailAddress(emailMessage.FromAddress))
            {
                throw new Exception("FromAddress must be set.");
            }

            if (emailMessage.ToAddresses.Any(x => !this.IsValidEmailAddress(x)))
            {
                throw new Exception("Invalid ToAddresses.");
            }

            if (emailMessage.CcAddresses.Any(x => !this.IsValidEmailAddress(x)))
            {
                throw new Exception("Invalid CcAddresses.");
            }

            if (emailMessage.BccAddresses.Any(x => !this.IsValidEmailAddress(x)))
            {
                throw new Exception("Invalid BccAddresses.");
            }

            if (!string.IsNullOrWhiteSpace(emailMessage.ReplyToAddress) && !this.IsValidEmailAddress(emailMessage.ReplyToAddress))
            {
                throw new Exception("Invalid ReplyToAddress.");
            }
        }

        private static HttpContent GetHttpContent(IEmailMessage emailMessage)
        {
            var personalizations = new Dictionary<string, object>
            {
                ["to"] = emailMessage.ToAddresses.Select(email => new { email }),
                ["subject"] = emailMessage.Subject
            };

            if (emailMessage.CcAddresses.Any())
            {
                personalizations["cc"] = emailMessage.CcAddresses.Select(email => new { email });
            }

            if (emailMessage.BccAddresses.Any())
            {
                personalizations["bcc"] = emailMessage.BccAddresses.Select(email => new { email });
            }

            var content = new List<dynamic>();

            if (!string.IsNullOrWhiteSpace(emailMessage.BodyPlainText))
            {
                content.Add(new
                {
                    type = "text/plain",
                    value = emailMessage.BodyPlainText
                });
            }

            if (!string.IsNullOrWhiteSpace(emailMessage.BodyHtml))
            {
                content.Add(new
                {
                    type = "text/html",
                    value = emailMessage.BodyHtml
                });
            }

            var message = new Dictionary<string, object>
            {
                ["personalizations"] = new List<object> { personalizations },
                ["from"] = new { email = emailMessage.FromAddress },
                ["content"] = content
            };

            if (!string.IsNullOrWhiteSpace(emailMessage.ReplyToAddress))
            {
                message["reply_to"] = new { email = emailMessage.ReplyToAddress };
            }

            return new JsonContent(message);
        }
    }
}
