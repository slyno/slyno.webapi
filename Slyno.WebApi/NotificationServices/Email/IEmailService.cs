﻿using System.Threading.Tasks;

namespace Slyno.WebApi.NotificationServices.Email
{
    public interface IEmailService
    {
        Task SendAsync(IEmailMessage emailMessage);

        bool IsValidEmailAddress(string emailAddress);
    }
}
