﻿using System.Collections.Generic;

namespace Slyno.WebApi.NotificationServices.Text
{
    public class TextMessage : ITextMessage
    {
        public string ToNumber { get; set; }

        public string FromNumber { get; set; }

        public string Body { get; set; }

        public string MediaUrl { get; set; }
    }
}
