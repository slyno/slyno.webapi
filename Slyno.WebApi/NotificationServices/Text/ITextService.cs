﻿using System.Threading.Tasks;

namespace Slyno.WebApi.NotificationServices.Text
{
    public interface ITextService
    {
        Task SendAsync(ITextMessage textMessage);
    }
}
