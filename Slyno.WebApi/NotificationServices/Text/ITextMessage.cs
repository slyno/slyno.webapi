﻿using System.Collections.Generic;

namespace Slyno.WebApi.NotificationServices.Text
{
    public interface ITextMessage
    {
        string ToNumber { get; }

        string FromNumber { get; }

        string Body { get; }

        string MediaUrl { get; }
    }
}
