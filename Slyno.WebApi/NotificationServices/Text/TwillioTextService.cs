﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Slyno.WebApi.NotificationServices.Text
{
    public class TwillioTextService : ITextService
    {
        private const string SendEndpoint = "https://api.twilio.com/2010-04-01/Accounts/{0}/Messages";

        private readonly string _accountSid;
        private readonly string _authToken;

        public TwillioTextService(string accountSid, string authToken)
        {
            _accountSid = accountSid;
            _authToken = authToken;
        }

        public async Task SendAsync(ITextMessage textMessage)
        {
            if (textMessage == null)
            {
                throw new Exception("TextMessage must not be null.");
            }

            var to = CleanNumber(textMessage.ToNumber);
            var from = CleanNumber(textMessage.FromNumber);

            var content = GetHttpContent(to, from, textMessage.Body, textMessage.MediaUrl);

            var uri = string.Format(SendEndpoint, _accountSid);

            using (var client = new HttpClient())
            {
                var byteArray = Encoding.ASCII.GetBytes($"{_accountSid}:{_authToken}");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                var response = await client.PostAsync(uri, content);

                if (!response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync();
                    throw new Exception($"Error sending email: {data}");
                }
            }
        }

        private static string CleanNumber(string number)
        {
            if (string.IsNullOrWhiteSpace(number))
            {
                return string.Empty;
            }

            var clean = Regex.Replace(number, @"\D", string.Empty);

            //we add the +1 if there isn't anything so the default country code is US.
            return number.StartsWith("+") ? $"+{clean}" : $"+1{clean}";
        }

        private static HttpContent GetHttpContent(string to, string from, string body, string mediaUrl)
        {
            if (string.IsNullOrWhiteSpace(to))
            {
                throw new Exception("Invalid ToNumber.");
            }

            if (string.IsNullOrWhiteSpace(from))
            {
                throw new Exception("Invalid FromNumber.");
            }

            if (string.IsNullOrWhiteSpace(body) && string.IsNullOrWhiteSpace(mediaUrl))
            {
                throw new Exception("Body or MediaUrl is required.");
            }

            var values = new Dictionary<string, string>
            {
                ["To"] = to,
                ["From"] = from
            };

            if (!string.IsNullOrWhiteSpace(body))
            {
                values["Body"] = body;
            }

            if (!string.IsNullOrWhiteSpace(mediaUrl))
            {
                Uri uri;
                if (!Uri.TryCreate(mediaUrl, UriKind.Absolute,  out uri))
                {
                    throw new Exception("MediaUrl must be a valid url ");
                }

                values["MediaUrl"] = mediaUrl;
            }

            return new FormUrlEncodedContent(values);
        }
    }
}
