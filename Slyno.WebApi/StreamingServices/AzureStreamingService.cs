﻿using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Slyno.WebApi.Exceptions;

namespace Slyno.WebApi.StreamingServices
{
    public abstract class AzureStreamingService : StreamingService
    {
        private readonly string _connectionString;

        protected AzureStreamingService(string connectionString)
        {
            _connectionString = connectionString;
        }

        private CloudBlobClient GetClient()
        {
            var storageAccount = CloudStorageAccount.Parse(_connectionString);

            return storageAccount.CreateCloudBlobClient();
        }

        private async Task<CloudBlobContainer> GetContainerAsync(IStorable storable)
        {
            if (string.IsNullOrWhiteSpace(storable.StorageFolder))
            {
                throw new BadRequestException("Invalid StorageFolder.");
            }

            //container names must be all lowercase per Azure specs. It will throw if it isn't
            var containerName = storable.StorageFolder.Trim().ToLower();

            var container = this.GetClient().GetContainerReference(containerName);

            await container.CreateIfNotExistsAsync();

            return container;
        }

        private async Task<CloudBlockBlob> GetBlobReferenceAsync(IStorable storable)
        {
            var container = await this.GetContainerAsync(storable);

            return container.GetBlockBlobReference(storable.StorageHandle.ToString());
        }

        public abstract override bool IsValidMediaType(string mediaType);

        public override async Task UploadAsync(Stream uploadStream, IStorable storable, bool overwrite = false)
        {
            var blockBlob = await this.GetBlobReferenceAsync(storable);

            if (!overwrite && await blockBlob.ExistsAsync())
            {
                throw new BadRequestException("File already exists.");
            }

            await blockBlob.UploadFromStreamAsync(uploadStream);
        }

        public override async Task<Stream> DownloadAsync(IStorable storable)
        {
            var blockBlob = await this.GetBlobReferenceAsync(storable);

            return await blockBlob.OpenReadAsync();
        }

        public override async Task DeleteAsync(IStorable storable)
        {
            var blockBlob = await this.GetBlobReferenceAsync(storable);

            await blockBlob.DeleteIfExistsAsync();
        }

        public override async Task<HttpResponseMessage> DownloadAsync(RangeHeaderValue range, IStorable storable)
        {
            try
            {
                return await base.DownloadAsync(range, storable);
            }
            catch (StorageException ex)
            {
                throw new HttpException(ex.RequestInformation.HttpStatusCode, ex.RequestInformation.HttpStatusMessage);
            }
        }
    }
}
