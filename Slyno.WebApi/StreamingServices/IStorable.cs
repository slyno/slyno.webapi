﻿using System;

namespace Slyno.WebApi.StreamingServices
{
    public interface IStorable
    {
        string StorageFolder { get; set; }

        Guid StorageHandle { get; set; }

        string MediaType { get; set; }

        string FileName { get; set; }

        long FileSize { get; set; }
    }
}
