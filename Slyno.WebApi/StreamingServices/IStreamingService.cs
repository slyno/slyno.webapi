﻿using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Slyno.WebApi.StreamingServices
{
    public interface IStreamingService
    {
        bool IsValidMediaType(string mediaType);

        Task UploadAsync(Stream uploadStream, IStorable storable, bool overwrite = false);

        Task<Stream> DownloadAsync(IStorable storable);

        Task DeleteAsync(IStorable storable);

        /// <summary>
        /// Gets an HttpResponseMessage with the download stream set as the content and all the response headers properly set.
        /// </summary>
        /// <param name="range">The range header. Pass null to return the entire file contents.</param>
        /// <param name="storable">The storable of the download.</param>
        Task<HttpResponseMessage> DownloadAsync(RangeHeaderValue range, IStorable storable);
    }
}
