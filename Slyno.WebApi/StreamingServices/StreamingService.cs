﻿using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Slyno.WebApi.Exceptions;

namespace Slyno.WebApi.StreamingServices
{
    public abstract class StreamingService : IStreamingService
    {
        public abstract bool IsValidMediaType(string mediaType);

        public abstract Task UploadAsync(Stream uploadStream, IStorable storable, bool overwrite = false);

        public abstract Task<Stream> DownloadAsync(IStorable storable);

        public abstract Task DeleteAsync(IStorable storable);

        public virtual async Task<HttpResponseMessage> DownloadAsync(RangeHeaderValue range, IStorable storable)
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Headers.AcceptRanges.Add("bytes");

            var contentTypeHeader = new MediaTypeHeaderValue(storable.MediaType);

            try
            {
                //don't use a using statement. The ASP.NET framework will dispose any HttpContent.

                var stream = await this.DownloadAsync(storable);

                if (range != null)
                {
                    response.StatusCode = HttpStatusCode.PartialContent;
                    response.Content = new ByteRangeStreamContent(stream, range, contentTypeHeader);
                }
                else
                {
                    response.Content = new StreamContent(stream);
                    response.Content.Headers.ContentType = contentTypeHeader;
                    response.Content.Headers.ContentLength = stream.Length;
                }

                return response;
            }
            catch (FileNotFoundException)
            {
                throw new NotFoundException();
            }
        }
    }
}
