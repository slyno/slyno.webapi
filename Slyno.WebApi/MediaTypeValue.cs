﻿namespace Slyno.WebApi
{
    public static class MediaTypeValue
    {
        public const string Any = "*/*";

        public const string ApplicationOctetStream = "application/octet-stream";

        public const string ApplicationJson = "application/json";

        public const string ImageJpeg = "image/jpeg";

        public const string ImageGif = "image/gif";

        public const string ImagePng = "image/png";

        public const string VideoMp4 = "video/mp4";
    }
}
