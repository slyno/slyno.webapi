﻿using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace Slyno.WebApi
{
    public class JsonContent : StringContent
    {
        private static readonly JsonSerializerSettings DefaultSerializerSettings = new JsonSerializerSettings
        {
            DateTimeZoneHandling = DateTimeZoneHandling.Utc,
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        };

        /// <summary>
        /// Creates HttpContent that will have the content type of application/json.
        /// </summary>
        /// <param name="entity">Typically pass a dynamic or simple object here. This will get converted to a JSON string. Any DateTimes will be UTC.</param>
        public JsonContent(object entity) : this(entity, DefaultSerializerSettings)
        {
        }

        /// <summary>
        /// Creates HttpContent that will have the content type of application/json.
        /// </summary>
        /// <param name="entity">Typically pass a dynamic or simple object here. This will get converted to a JSON string.</param>
        /// <param name="settings">Serializer settings to use.</param>
        public JsonContent(object entity, JsonSerializerSettings settings) : base(JsonConvert.SerializeObject(entity, settings))
        {
            this.Headers.ContentType = new MediaTypeHeaderValue(MediaTypeValue.ApplicationJson);
        }
    }
}
