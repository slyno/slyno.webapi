﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Slyno.WebApi.Data.Crud;
using Slyno.WebApi.Data.Models;
using Slyno.WebApi.Data.Rules;
using Slyno.WebApi.Exceptions;
using Slyno.WebApi.StreamingServices;
using UnsupportedMediaTypeException = Slyno.WebApi.Exceptions.UnsupportedMediaTypeException;

namespace Slyno.WebApi.Controllers
{
    public abstract class FileApiController<T> : ModelApiController<T> where T : class, IModel, IStorable
    {
        protected FileApiController(ICrudService crudService) : base(crudService)
        {
        }

        /// <summary>
        /// Processes a binary download from the given model using the streaming service provided by GetStreamingService.
        /// This will set the cache based on the given TimeSpan.
        /// This method doesn't do any permissions checks on the model.
        /// </summary>
        /// <param name="model">Model to download</param>
        /// <param name="timeToCache">TimeSpan to set the cache header to.</param>
        protected async Task<HttpResponseMessage> DownloadAsync(T model, TimeSpan timeToCache)
        {
            var streamingService = this.GetStreamingService(model);

            var range = this.Request.Headers.Range;

            var response = await streamingService.DownloadAsync(range, model);

            response.Headers.CacheControl = new CacheControlHeaderValue
            {
                NoCache = !(timeToCache.TotalMilliseconds > 0),
                Private = true,
                MaxAge = timeToCache
            };

            return response;
        }

        /// <summary>
        /// Validates the model can be created, uploads the file, then creates the model in the database.
        /// The new model is returned that will have any database generated values in tact.
        /// </summary>
        /// <param name="model">Model to upload and create. Make sure FileName has been set. MediaType and FileSize will be set based on the request.</param>
        /// <param name="executeBusinessRules"></param>
        /// <param name="overwrite"></param>
        protected async Task<T> UploadAsync(T model, bool executeBusinessRules = true, bool overwrite = false)
        {
            var httpContent = this.Request.Content;

            if (httpContent.IsMimeMultipartContent())
            {
                throw new BadRequestException("Only binary uploads are accepted.");
            }

            var streamingService = this.GetStreamingService(model);

            model.MediaType = httpContent.Headers.ContentType?.MediaType;

            if (string.IsNullOrWhiteSpace(model.MediaType) || !streamingService.IsValidMediaType(model.MediaType))
            {
                throw new UnsupportedMediaTypeException();
            }

            //this is for pre-checking the user can create the model
            //we don't want to create the model without uploading the data first.
            await this.CrudService.ValidateCanCreateAsync(model, RuleType.DataRule, this.ApiPrincipal);

            if (executeBusinessRules)
            {
                await this.CrudService.ValidateCanCreateAsync(model, RuleType.DataRule, this.ApiPrincipal);
            }

            using (var stream = await httpContent.ReadAsStreamAsync())
            {
                if (stream == null || stream.Length == 0)
                {
                    throw new BadRequestException("Invalid binary data.");
                }

                model.FileSize = stream.Length;

                await streamingService.UploadAsync(stream, model);
            }

            try
            {
                await this.CreateAsync(model);

                return model;
            }
            catch
            {
                try
                {
                    await streamingService.DeleteAsync(model);
                }
                catch
                {
                    this.Logger?.Log($"Error creating {typeof(T).Name} and unable to delete file {model.StorageFolder}/{model.StorageHandle}");
                }

                throw;
            }
        }

        protected virtual IStreamingService GetStreamingService(T model)
        {
            throw new ServiceUnavailableException("Unable to connect to streaming service.");
        }
    }
}
