﻿using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Slyno.WebApi.Data.Crud;
using Slyno.WebApi.Data.Models;

namespace Slyno.WebApi.Controllers
{
    public abstract class ModelApiController<T> : BaseApiController where T : class, IModel
    {
        protected ModelApiController(ICrudService crudService)
        {
            this.CrudService = crudService;
        }

        protected ICrudService CrudService { get; }

        protected virtual async Task CreateAsync(T model, bool executeBusinessRules = true)
        {
            await this.CrudService.CreateAsync(model, this.ApiPrincipal, executeBusinessRules);
        }

        protected virtual async Task UpdateAsync(T model, JObject delta, bool executeBusinessRules = true, bool honorNonPatchable = true)
        {
            await this.CrudService.UpdateAsync(model, delta, this.ApiPrincipal, executeBusinessRules, honorNonPatchable);
        }

        protected virtual async Task DeleteAsync(T model, bool executeBusinessRules = true, bool honorNonDeleteable = true)
        {
            await this.CrudService.DeleteAsync(model, this.ApiPrincipal, executeBusinessRules, honorNonDeleteable);
        }
    }
}
