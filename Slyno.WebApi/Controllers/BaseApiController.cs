﻿using System.Web.Http;
using Slyno.WebApi.Auth;
using Slyno.WebApi.Extensions;
using Slyno.WebApi.Loggers;

namespace Slyno.WebApi.Controllers
{
    public abstract class BaseApiController : ApiController
    {
        public IApiPrincipal ApiPrincipal => this.GetApiPrincipal();

        public ILogger Logger { get; set; }
    }
}
