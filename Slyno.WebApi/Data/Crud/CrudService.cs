﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Slyno.WebApi.Auth;
using Slyno.WebApi.Data.Attributes;
using Slyno.WebApi.Data.Models;
using Slyno.WebApi.Data.Rules;
using Slyno.WebApi.Exceptions;
using Slyno.WebApi.Loggers;

namespace Slyno.WebApi.Data.Crud
{
    public abstract class CrudService : ICrudService
    {
        private readonly IPatcher _patcher;
        private readonly IRulesService _rulesService;

        protected CrudService(IPatcher patcher, IRulesService rulesService)
        {
            _patcher = patcher;
            _rulesService = rulesService;
        }

        public ILogger Logger { get; set; }

        public virtual async Task<T> ReadSingleAsync<T>(int id) where T : class, IModel
        {
            var model = await this.ReadSingleOrDefaultAsync<T>(id);
            if (model == null)
            {
                throw new NotFoundException($"{typeof(T).Name} was not found with Id: {id}.");
            }
            return model;
        }

        public virtual async Task<T> ReadSingleAsync<T>(Guid uniqueId) where T : class, IModel, IUnique
        {
            var model = await this.ReadSingleOrDefaultAsync<T>(uniqueId);
            if (model == null)
            {
                throw new NotFoundException($"{typeof(T).Name} was not found with UniqueId: {uniqueId}.");
            }
            return model;
        }

        public virtual async Task<T> ReadSingleAsync<T>(string token) where T : class, IModel, ITokenizable
        {
            var model = await this.ReadSingleOrDefaultAsync<T>(token);
            if (model == null)
            {
                throw new NotFoundException($"{typeof(T).Name} was not found with Token: {token}.");
            }
            return model;
        }

        public abstract Task<T> ReadSingleOrDefaultAsync<T>(int id) where T : class, IModel;

        public abstract Task<T> ReadSingleOrDefaultAsync<T>(Guid uniqueId) where T : class, IModel, IUnique;

        public abstract Task<T> ReadSingleOrDefaultAsync<T>(string token) where T : class, IModel, ITokenizable;

        public abstract IQueryable<T> ReadCollection<T>() where T : class, IModel;

        public virtual async Task CreateAsync<T>(T model, IApiPrincipal principal, bool executeBusinessRules = true) where T : class, IModel
        {
            await this.ValidateCanCreateAsync(model, RuleType.DataRule, principal);

            if (executeBusinessRules)
            {
                await this.ValidateCanCreateAsync(model, RuleType.BusinessRule, principal);
            }

            var creatable = model as ICreatable;

            if (creatable != null)
            {
                creatable.CreatedByUserId = principal?.User?.Id;
                creatable.CreatedDate = DateTime.UtcNow;
            }

            var beforeCrudRules = this.GetCreateRules<T>(RuleType.BeforeCrudRule);
            foreach (var beforeCrudRule in beforeCrudRules)
            {
                await beforeCrudRule.ExecuteAsync(model, this, principal);
            }

            await this.PersistCreateAsync(model);

            //get the model fresh from persisted storage so all properties are set
            model = await this.ReadSingleAsync<T>(model.Id);

            var crudRules = this.GetCreateRules<T>(RuleType.CrudRule);

            foreach (var crudRule in crudRules)
            {
                try
                {
                    await crudRule.ExecuteAsync(model, this, principal);
                }
                catch (Exception ex)
                {
                    this.Logger?.Log($"Error executing CreateRule {crudRule.GetType().AssemblyQualifiedName}{Environment.NewLine}{ex}");
                }
            }
        }

        public virtual async Task<PatchResult> UpdateAsync<T>(T model, JObject delta, IApiPrincipal principal, bool executeBusinessRules = true,
            bool honorNonPatchable = true) where T : class, IModel
        {
            var prePatchResult = new PatchResult();
            foreach (var change in delta)
            {
                prePatchResult.NewValues.Add(change.Key, change.Value);
            }

            await this.ValidateCanUpdateAsync(model, RuleType.DataRule, prePatchResult, principal);

            if (executeBusinessRules)
            {
                await this.ValidateCanUpdateAsync(model, RuleType.BusinessRule, prePatchResult, principal);
            }

            var patchResult = _patcher.Apply(model, delta, honorNonPatchable);
            if (!patchResult.NewValues.Any())
            {
                return patchResult;
            }

            var updateable = model as IUpdateable;

            if (updateable != null)
            {
                updateable.UpdatedByUserId = principal?.User?.Id;
                updateable.UpdatedDate = DateTime.UtcNow;
            }

            var beforeCrudRules = this.GetUpdateRules<T>(RuleType.BeforeCrudRule);
            foreach (var beforeCrudRule in beforeCrudRules)
            {
                await beforeCrudRule.ExecuteAsync(model, prePatchResult, this, principal);
            }

            await this.PersistUpdateAsync(model);

            var crudRules = this.GetUpdateRules<T>(RuleType.CrudRule);

            foreach (var crudRule in crudRules)
            {
                try
                {
                    await crudRule.ExecuteAsync(model, patchResult, this, principal);
                }
                catch (Exception ex)
                {
                    this.Logger?.Log($"Error executing UpdateRule {crudRule.GetType().AssemblyQualifiedName}{Environment.NewLine}{ex}");
                }
            }

            return patchResult;
        }

        public async Task<PatchResult> UpdateAsync<T>(T model, IDictionary<string, object> delta, IApiPrincipal principal, bool executeBusinessRules = true,
            bool honorNonPatchable = true) where T : class, IModel
        {
            return await this.UpdateAsync(model, _patcher.GetDelta(delta), principal, executeBusinessRules, honorNonPatchable);
        }

        public virtual async Task DeleteAsync<T>(T model, IApiPrincipal principal, bool executeBusinessRules = true, bool honorNonDeletable = true) where T : class, IModel
        {
            if (honorNonDeletable)
            {
                if (typeof(T).IsDefined(typeof(NonDeletableAttribute), true))
                {
                    throw new ForbiddenException("This type of object can't be deleted.");
                }
            }

            await this.ValidateCanDeleteAsync(model, RuleType.DataRule, principal);

            if (executeBusinessRules)
            {
                await this.ValidateCanDeleteAsync(model, RuleType.BusinessRule, principal);
            }

            var beforeCrudRules = this.GetDeleteRules<T>(RuleType.BeforeCrudRule);
            foreach (var beforeCrudRule in beforeCrudRules)
            {
                await beforeCrudRule.ExecuteAsync(model, this, principal);
            }

            await this.PersistDeleteAsync(model);

            var crudRules = this.GetDeleteRules<T>(RuleType.CrudRule);

            foreach (var crudRule in crudRules)
            {
                try
                {
                    await crudRule.ExecuteAsync(model, this, principal);
                }
                catch (Exception ex)
                {
                    this.Logger?.Log($"Error executing DeleteRule {crudRule.GetType().AssemblyQualifiedName}{Environment.NewLine}{ex}");
                }
            }
        }

        /// <summary>
        /// The model has passed all required rules and is ready to be persisted to storage.
        /// </summary>
        /// <typeparam name="T">Type of model.</typeparam>
        /// <param name="model">Model to be persisted.</param>
        protected abstract Task PersistCreateAsync<T>(T model) where T : class, IModel;

        /// <summary>
        /// The model has passed all required rules and has been updated and ready to be persisted to storage.
        /// </summary>
        /// <typeparam name="T">Type of model.</typeparam>
        /// <param name="model">Model to be persisted.</param>
        protected abstract Task PersistUpdateAsync<T>(T model) where T : class, IModel;

        /// <summary>
        /// The model has passed all required rules and is ready to be deleted from storage.
        /// </summary>
        /// <typeparam name="T">Type of model.</typeparam>
        /// <param name="model">Model to be persisted.</param>
        protected abstract Task PersistDeleteAsync<T>(T model) where T : class, IModel;

        public virtual async Task SortCollectionAsync<T>(IQueryable<T> collection, int[] ids, IApiPrincipal principal) where T : class, IModel, ISortable
        {
            if (ids.Length != collection.Count())
            {
                throw new Exception("Invalid ids were sent to be sorted.");
            }

            var sort = 0;

            foreach (var id in ids)
            {
                var item = collection.FirstOrDefault(x => x.Id == id);
                if (item == null)
                {
                    continue;
                }

                var changes = new Dictionary<string, object>
                {
                    {nameof(item.Sort), sort}
                };

                await this.UpdateAsync(item, changes, principal, true, false);

                sort++;
            }
        }

        public virtual async Task ValidateCanCreateAsync<T>(T model, RuleType ruleType, IApiPrincipal principal) where T : class, IModel
        {
            var rules = this.GetCreateRules<T>(ruleType);

            foreach (var rule in rules)
            {
                await rule.ExecuteAsync(model, this, principal);
            }
        }

        public virtual async Task ValidateCanUpdateAsync<T>(T model, RuleType ruleType, PatchResult patchResult, IApiPrincipal principal) where T : class, IModel
        {
            var rules = this.GetUpdateRules<T>(ruleType);

            foreach (var rule in rules)
            {
                await rule.ExecuteAsync(model, patchResult, this, principal);
            }
        }

        public virtual async Task ValidateCanDeleteAsync<T>(T model, RuleType ruleType, IApiPrincipal principal) where T : class, IModel
        {
            var rules = this.GetDeleteRules<T>(ruleType);

            foreach (var rule in rules)
            {
                await rule.ExecuteAsync(model, this, principal);
            }
        }

        protected IEnumerable<ICreateRule<T>> GetCreateRules<T>(RuleType type) where T : IModel
        {
            return _rulesService.GetCreateRules<T>(type);
        }

        protected IEnumerable<IUpdateRule<T>> GetUpdateRules<T>(RuleType type) where T : IModel
        {
            return _rulesService.GetUpdateRules<T>(type);
        }

        protected IEnumerable<IDeleteRule<T>> GetDeleteRules<T>(RuleType type) where T : IModel
        {
            return _rulesService.GetDeleteRules<T>(type);
        }
    }
}
