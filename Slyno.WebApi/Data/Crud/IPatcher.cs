﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Slyno.WebApi.Data.Models;

namespace Slyno.WebApi.Data.Crud
{
    public interface IPatcher
    {
        PatchResult Apply<TModel>(TModel model, JObject delta, bool honerNonPatchable = true) where TModel : IModel;

        PatchResult Apply<TModel>(TModel model, IDictionary<string, object> delta, bool honerNonPatchable = true) where TModel : IModel;

        JObject GetDelta(IDictionary<string, object> delta);
    }
}
