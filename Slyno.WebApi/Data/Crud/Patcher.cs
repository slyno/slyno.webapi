﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json.Linq;
using Slyno.WebApi.Data.Attributes;
using Slyno.WebApi.Data.Models;
using Slyno.WebApi.Exceptions;

namespace Slyno.WebApi.Data.Crud
{
    public class Patcher : IPatcher
    {
        private readonly ConcurrentDictionary<Type, List<PropertyInfo>> _propertyCache = new ConcurrentDictionary<Type, List<PropertyInfo>>();
        private readonly ConcurrentDictionary<Type, List<PropertyInfo>> _nonPatchablePropertyCache = new ConcurrentDictionary<Type, List<PropertyInfo>>();

        public PatchResult Apply<TModel>(TModel model, JObject delta, bool honerNonPatchable = true) where TModel : IModel
        {
            var finalDelta = this.GetFinalDelta<TModel>(delta, honerNonPatchable);

            var deltaResult = new PatchResult();

            var validationErrors = new List<string>();

            var settableProperties = this.GetSettableProperties<TModel>();

            foreach (var kvp in finalDelta)
            {
                var key = kvp.Key;
                var newValue = kvp.Value;

                var propToSet = settableProperties.FirstOrDefault(x => x.Name == key);

                if (propToSet == null)
                {
                    //should never happen since the constructor did all the checking.
                    validationErrors.Add($"Invalid property: {key}");
                    continue;
                }

                try
                {
                    var oldValue = propToSet.GetValue(model);

                    if (oldValue == null && newValue == null)
                    {
                        continue;
                    }

                    if (oldValue != null && oldValue.Equals(newValue))
                    {
                        continue;
                    }

                    //get the old value
                    deltaResult.OldValues.Add(key, oldValue);

                    //attempt to set the property
                    propToSet.SetValue(model, newValue);

                    //in case the model alters the value at all during a setter, pull it fresh.
                    deltaResult.NewValues.Add(key, propToSet.GetValue(model));
                }
                catch (Exception ex)
                {
                    var message = ex.InnerException?.Message ?? ex.Message;

                    validationErrors.Add($"Error updating {kvp.Key}: {message}");
                }
            }

            if (validationErrors.Any())
            {
                throw new BadRequestException(string.Join(Environment.NewLine, validationErrors));
            }

            return deltaResult;
        }

        public PatchResult Apply<TModel>(TModel model, IDictionary<string, object> delta, bool honerNonPatchable = true) where TModel : IModel
        {
            var json = this.GetDelta(delta);

            return this.Apply(model, json, honerNonPatchable);
        }

        public JObject GetDelta(IDictionary<string, object> values)
        {
            var jobject = new JObject();

            foreach (var value in values)
            {
                jobject.Add(value.Key, JToken.FromObject(value.Value));
            }

            return jobject;
        }

        private IDictionary<string, object> GetFinalDelta<TModel>(JObject delta, bool honorNonPatchable)
        {
            var finalDelta = new Dictionary<string, object>();

            var modelType = typeof(TModel);

            if (honorNonPatchable && Attribute.IsDefined(modelType, typeof(NonPatchableAttribute), true))
            {
                throw new ApiException("Object is read-only.");
            }

            var settableProperties = this.GetSettableProperties<TModel>();

            var nonPatchableProperties = this.GetNonPatchableProperties<TModel>();

            var validationErrors = new List<string>();

            foreach (var jsonProperty in delta)
            {
                var propInfo = settableProperties.FirstOrDefault(x => string.Equals(x.Name, jsonProperty.Key, StringComparison.CurrentCultureIgnoreCase));

                if (null == propInfo)
                {
                    validationErrors.Add($"Invalid or read-only property: {jsonProperty.Key}");
                    continue;
                }

                if (finalDelta.ContainsKey(propInfo.Name))
                {
                    validationErrors.Add($"Propery declared more than once: {jsonProperty.Key}");
                    continue;
                }

                if (honorNonPatchable && nonPatchableProperties.Contains(propInfo))
                {
                    validationErrors.Add($"Invalid or read-only property: {jsonProperty.Key}");
                }

                var type = propInfo.PropertyType;

                try
                {
                    //this will ensure the type is casted correctly since double/int can have bad casting.
                    finalDelta.Add(propInfo.Name, jsonProperty.Value.ToObject(type));
                }
                catch
                {
                    validationErrors.Add($"Unable to cast {jsonProperty.Key} to {type.Name}");
                }
            }

            if (validationErrors.Any())
            {
                throw new BadRequestException(string.Join(Environment.NewLine, validationErrors));
            }

            return finalDelta;
        }

        private List<PropertyInfo> GetSettableProperties<TModel>()
        {
            var type = typeof(TModel);

            List<PropertyInfo> settableProperties;

            if (_propertyCache.TryGetValue(type, out settableProperties))
            {
                return settableProperties;
            }

            settableProperties = type.GetProperties().Where(x => x.CanWrite && x.GetSetMethod() != null).ToList();

            _propertyCache.TryAdd(type, settableProperties);

            return settableProperties;
        }

        private List<PropertyInfo> GetNonPatchableProperties<TModel>()
        {
            var type = typeof(TModel);

            List<PropertyInfo> nonPatchableProperties;

            if (_nonPatchablePropertyCache.TryGetValue(type, out nonPatchableProperties))
            {
                return nonPatchableProperties;
            }

            nonPatchableProperties = this.GetSettableProperties<TModel>()
                .Where(x => Attribute.IsDefined(x, typeof(NonPatchableAttribute), true))
                .ToList();

            _nonPatchablePropertyCache.TryAdd(type, nonPatchableProperties);

            return nonPatchableProperties;
        }
    }
}
