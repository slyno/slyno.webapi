﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Slyno.WebApi.Auth;
using Slyno.WebApi.Data.Models;
using Slyno.WebApi.Data.Rules;
using Slyno.WebApi.Loggers;

namespace Slyno.WebApi.Data.Crud
{
    public interface ICrudService
    {
        ILogger Logger { get; set; }

        Task<T> ReadSingleAsync<T>(int id) where T : class, IModel;
        Task<T> ReadSingleAsync<T>(Guid uniqueId) where T : class, IModel, IUnique;
        Task<T> ReadSingleAsync<T>(string token) where T : class, IModel, ITokenizable;

        Task<T> ReadSingleOrDefaultAsync<T>(int id) where T : class, IModel;
        Task<T> ReadSingleOrDefaultAsync<T>(Guid uniqueId) where T : class, IModel, IUnique;
        Task<T> ReadSingleOrDefaultAsync<T>(string token) where T : class, IModel, ITokenizable;

        IQueryable<T> ReadCollection<T>() where T : class, IModel;

        Task CreateAsync<T>(T model, IApiPrincipal principal, bool executeBusinessRules = true) where T : class, IModel;

        Task<PatchResult> UpdateAsync<T>(T model, JObject delta, IApiPrincipal principal, bool executeBusinessRules = true, bool honorNonPatchable = true) where T : class, IModel;

        Task<PatchResult> UpdateAsync<T>(T model, IDictionary<string, object> delta, IApiPrincipal principal, bool executeBusinessRules = true, bool honorNonPatchable = true) where T : class, IModel;

        Task DeleteAsync<T>(T model, IApiPrincipal principal, bool executeBusinessRules = true, bool honorNonDeletable = true) where T : class, IModel;

        Task SortCollectionAsync<T>(IQueryable<T> collection, int[] ids, IApiPrincipal principal) where T : class, IModel, ISortable;


        Task ValidateCanCreateAsync<T>(T model, RuleType ruleType, IApiPrincipal principal) where T : class, IModel;

        Task ValidateCanUpdateAsync<T>(T model, RuleType ruleType, PatchResult patchResult, IApiPrincipal principal) where T : class, IModel;

        Task ValidateCanDeleteAsync<T>(T model, RuleType ruleType, IApiPrincipal principal) where T : class, IModel;
    }
}
