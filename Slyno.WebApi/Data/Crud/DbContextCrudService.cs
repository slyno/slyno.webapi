﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Slyno.WebApi.Data.Attributes;
using Slyno.WebApi.Data.Rules;

namespace Slyno.WebApi.Data.Crud
{
    public class DbContextCrudService : CrudService
    {
        private readonly DbContext _dbContext;

        private readonly ConcurrentDictionary<Type, IEnumerable<string>> _includes = new ConcurrentDictionary<Type, IEnumerable<string>>();

        public DbContextCrudService(DbContext dbContext, IPatcher patcher, IRulesService rulesService) : base(patcher, rulesService)
        {
            _dbContext = dbContext;
        }

        public override async Task<T> ReadSingleOrDefaultAsync<T>(int id)
        {
            return await this.ReadCollection<T>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public override async Task<T> ReadSingleOrDefaultAsync<T>(Guid uniqueId)
        {
            return await this.ReadCollection<T>().FirstOrDefaultAsync(x => x.UniqueId == uniqueId);
        }

        public override async Task<T> ReadSingleOrDefaultAsync<T>(string token)
        {
            var model = await this.ReadCollection<T>().FirstOrDefaultAsync(x => x.Token == token);
            if (model == null)
            {
                return null;
            }

            //ensures a case sensitive token
            return string.Equals(model.Token, token) ? model : null;
        }

        public override IQueryable<T> ReadCollection<T>()
        {
            var includes = this.GetIncludes(typeof(T), string.Empty);

            IQueryable<T> collection = _dbContext.Set<T>();

            return includes.Aggregate(collection, (current, name) => current.Include(name));
        }

        protected override async Task PersistCreateAsync<T>(T model)
        {
            _dbContext.Set<T>().Add(model);

            await _dbContext.SaveChangesAsync();
        }

        protected override async Task PersistUpdateAsync<T>(T model)
        {
            await _dbContext.SaveChangesAsync();
        }

        protected override async Task PersistDeleteAsync<T>(T model)
        {
            await _dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Uses reflection to look at the Model and find any properties
        /// with the IncludeAttribute and then adds it to the
        /// list of includes so EF can pull the entire object with navigation properties from the database.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="baseName"></param>
        private IEnumerable<string> GetIncludes(Type type, string baseName)
        {
            if (_includes.ContainsKey(type))
            {
                return _includes[type];
            }

            var list = new List<string>();

            var properties = type.GetProperties().Where(x => x.IsDefined(typeof(IncludeAttribute), true));

            foreach (var property in properties)
            {
                var fullName = string.Concat(baseName, property.Name);
                list.Add(fullName);

                var nextBaseName = string.Concat(fullName, ".");

                list.AddRange(this.GetIncludes(property.PropertyType, nextBaseName));
            }

            _includes.TryAdd(type, list);

            return _includes[type];
        }
    }
}
