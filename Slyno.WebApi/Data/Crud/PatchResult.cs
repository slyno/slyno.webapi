﻿using System.Collections.Generic;

namespace Slyno.WebApi.Data.Crud
{
    public class PatchResult
    {
        public PatchResult()
        {
            this.OldValues = new Dictionary<string, object>();
            this.NewValues = new Dictionary<string, object>();
        }

        public IDictionary<string, object> OldValues { get; }

        public IDictionary<string, object> NewValues { get; }
    }
}
