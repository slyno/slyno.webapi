﻿using System;

namespace Slyno.WebApi.Data.Attributes
{
    /// <summary>
    /// Any property within this object will be added to the Include statement.
    /// </summary>
    public sealed class IncludeAttribute : Attribute
    {
    }
}
