﻿using System;

namespace Slyno.WebApi.Data.Attributes
{
    /// <summary>
    /// Any property with this attribute will not be patchable.
    /// </summary>
    public sealed class NonPatchableAttribute : Attribute
    {
    }
}
