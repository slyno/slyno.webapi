﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Slyno.WebApi.Data.Attributes
{
    public class ModelValidationFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var modelState = actionContext.ModelState;

            if (modelState.IsValid)
            {
                return;
            }

            var errors = modelState.Values.Where(x => x.Errors.Any())
                .SelectMany(x => x.Errors)
                .Select(x => string.IsNullOrWhiteSpace(x.ErrorMessage) ? "Null or empty is not allowed." : x.ErrorMessage)
                .ToList();

            var errorResponse = new
            {
                Message = "Validation Failed",
                Errors = errors
            };

            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, errorResponse);
        }
    }
}
