﻿using System;

namespace Slyno.WebApi.Data.Attributes
{
    /// <summary>
    /// Any object with this attribute will not be deletable.
    /// </summary>
    public sealed class NonDeletableAttribute : Attribute
    {
    }
}
