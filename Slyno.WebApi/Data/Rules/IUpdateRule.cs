using System.Threading.Tasks;
using Slyno.WebApi.Auth;
using Slyno.WebApi.Data.Crud;
using Slyno.WebApi.Data.Models;

namespace Slyno.WebApi.Data.Rules
{
    /// <summary>
    /// For a DataRule, BusinessRule, and BeforeCrudRule, the model is about to be updated.
    /// For a CrudRule, the model has been updated.
    /// Navigation properties are available.
    /// PatchResult will only have the NewValues for CrudRules.
    /// </summary>
    public interface IUpdateRule<in T> where T : IModel
    {
        Task ExecuteAsync(T model, PatchResult patchResult, ICrudService crudService, IApiPrincipal principal);
    }
}
