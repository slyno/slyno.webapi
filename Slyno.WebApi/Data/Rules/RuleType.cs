﻿namespace Slyno.WebApi.Data.Rules
{
    public enum RuleType
    {
        /// <summary>
        /// Rules to enforce data integrity.
        /// This type of rule typically doesn't need to know anything about the current user.
        /// </summary>
        DataRule,

        /// <summary>
        /// Rules to enforce business logic.
        /// This type of rule will typically be based on the current user.
        /// </summary>
        BusinessRule,

        /// <summary>
        /// Rules to execute right before a CRUD operation occurs.
        /// Example would be to send an email before creating the model.
        /// </summary>
        BeforeCrudRule,

        /// <summary>
        /// Rules to exucte right after a CRUD operation occured.
        /// </summary>
        CrudRule
    }
}
