﻿using System;

namespace Slyno.WebApi.Data.Rules
{
    /// <summary>
    /// Executed after before entity has been created, updated, or deleted. BeforeCrudRules can't be skipped.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class BeforeCrudRuleAttribute : RuleAttribute
    {
    }
}
