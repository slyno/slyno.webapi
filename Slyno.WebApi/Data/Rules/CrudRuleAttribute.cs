﻿using System;

namespace Slyno.WebApi.Data.Rules
{
    /// <summary>
    /// A crud rule is executed after the entity has been created, updated, or deleted. Crud rules can't be skipped.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class CrudRuleAttribute : RuleAttribute
    {
    }
}
