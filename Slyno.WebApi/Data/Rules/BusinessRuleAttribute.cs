﻿using System;

namespace Slyno.WebApi.Data.Rules
{
    /// <summary>
    /// A business rule is validation based on the current user. Business rules can be skipped using the correct method signature.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class BusinessRuleAttribute : RuleAttribute
    {
    }
}
