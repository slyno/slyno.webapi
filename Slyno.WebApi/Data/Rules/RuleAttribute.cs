﻿using System;

namespace Slyno.WebApi.Data.Rules
{
    public abstract class RuleAttribute : Attribute
    {
        public static Type GetRuleAttributeType(RuleType ruleType)
        {
            switch (ruleType)
            {
                case RuleType.DataRule:
                    return typeof(DataRuleAttribute);
                case RuleType.BusinessRule:
                    return typeof(BusinessRuleAttribute);
                case RuleType.BeforeCrudRule:
                    return typeof(BeforeCrudRuleAttribute);
                case RuleType.CrudRule:
                    return typeof(CrudRuleAttribute);
                default:
                    throw new ArgumentOutOfRangeException(nameof(ruleType), ruleType, null);
            }
        }
    }
}
