﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Slyno.WebApi.Data.Rules
{
    public sealed class RuleFinder
    {
        private readonly Assembly _assembly;

        public RuleFinder(Assembly assembly)
        {
            _assembly = assembly;
        }

        public IEnumerable<Type> FindCreateRules()
        {
            return this.Find(typeof(ICreateRule<>));
        }

        public IEnumerable<Type> FindUpdateRules()
        {
            return this.Find(typeof(IUpdateRule<>));
        }

        public IEnumerable<Type> FindDeleteRules()
        {
            return this.Find(typeof(IDeleteRule<>));
        }

        private IEnumerable<Type> Find(Type type)
        {
            var rules = _assembly.GetTypes().Where(x => !x.IsAbstract &&
                x.GetInterfaces().Any(y => y.IsGenericType && y.GetGenericTypeDefinition() == type))
                .ToList();

            var invalidRules = rules.Where(x => !Attribute.IsDefined(x, typeof(DataRuleAttribute), true) &&
                                                            !Attribute.IsDefined(x, typeof(BusinessRuleAttribute), true) &&
                                                            !Attribute.IsDefined(x, typeof(BeforeCrudRuleAttribute), true) &&
                                                            !Attribute.IsDefined(x, typeof(CrudRuleAttribute), true)).ToList();

            if (invalidRules.Any())
            {
                var names = string.Join(Environment.NewLine, invalidRules.Select(x => x.FullName));
                throw new Exception($"Missing RuleAttribute on class {Environment.NewLine}{names}");
            }

            return rules;
        }
    }
}
