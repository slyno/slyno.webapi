﻿using System.Collections.Generic;
using Slyno.WebApi.Data.Models;

namespace Slyno.WebApi.Data.Rules
{
    public interface IRulesService
    {
        IEnumerable<ICreateRule<T>> GetCreateRules<T>(RuleType type) where T : IModel;

        IEnumerable<IUpdateRule<T>> GetUpdateRules<T>(RuleType type) where T : IModel;

        IEnumerable<IDeleteRule<T>> GetDeleteRules<T>(RuleType type) where T : IModel;
    }
}
