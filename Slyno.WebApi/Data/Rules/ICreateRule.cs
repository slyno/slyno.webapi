﻿using System.Threading.Tasks;
using Slyno.WebApi.Auth;
using Slyno.WebApi.Data.Crud;
using Slyno.WebApi.Data.Models;

namespace Slyno.WebApi.Data.Rules
{
    /// <summary>
    /// For a DataRule, BusinessRule, and BeforeCrudRule, the model is about to be created and navigation properties will be null.
    /// For a CrudRule, the model has been created and navigation properties are available.
    /// </summary>
    public interface ICreateRule<in T> where T : IModel
    {
        Task ExecuteAsync(T model, ICrudService crudService, IApiPrincipal principal);
    }
}
