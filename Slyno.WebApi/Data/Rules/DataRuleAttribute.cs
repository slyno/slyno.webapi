﻿using System;

namespace Slyno.WebApi.Data.Rules
{
    /// <summary>
    /// A data rule is used to validate data integrity. Data rules can't be skipped.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class DataRuleAttribute : RuleAttribute
    {
    }
}
