﻿using System;

namespace Slyno.WebApi.Data.Models
{
    public interface IUpdateable
    {
        int? UpdatedByUserId { get; set; }

        DateTime UpdatedDate { get; set; }
    }
}
