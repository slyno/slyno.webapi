﻿using System;

namespace Slyno.WebApi.Data.Models
{
    public interface IExpirable
    {
        DateTime StartDate { get; set; }

        DateTime? EndDate { get; set; }
    }
}
