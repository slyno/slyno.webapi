﻿namespace Slyno.WebApi.Data.Models
{
    public interface IModel
    {
        int Id { get; set; }
    }
}
