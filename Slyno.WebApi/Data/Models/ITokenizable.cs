﻿namespace Slyno.WebApi.Data.Models
{
    public interface ITokenizable
    {
        string Token { get; set; }
    }
}
