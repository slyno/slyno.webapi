﻿using System;

namespace Slyno.WebApi.Data.Models
{
    public interface ICreatable
    {
        int? CreatedByUserId { get; set; }

        DateTime CreatedDate { get; set; }
    }
}
