﻿using System;

namespace Slyno.WebApi.Data.Models
{
    public interface IUnique
    {
        Guid UniqueId { get; set; }
    }
}
