﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Slyno.WebApi.Data.Attributes;

namespace Slyno.WebApi.Data.Models
{
    public abstract class Model : IModel
    {
        private readonly Dictionary<string, object> _propertyBackingDictionary = new Dictionary<string, object>();

        [NonPatchable]
        public int Id { get; set; }

        protected T GetPropertyValue<T>([CallerMemberName] string propertyName = null)
        {
            if (propertyName == null)
            {
                throw new ArgumentNullException(nameof(propertyName));
            }

            object value;

            if (_propertyBackingDictionary.TryGetValue(propertyName, out value))
            {
                return (T)value;
            }

            return default(T);
        }

        protected bool SetPropertyValue<T>(T newValue, [CallerMemberName] string propertyName = null)
        {
            if (propertyName == null)
            {
                throw new ArgumentNullException(nameof(propertyName));
            }

            // ReSharper disable once ExplicitCallerInfoArgument
            if (EqualityComparer<T>.Default.Equals(newValue, GetPropertyValue<T>(propertyName)))
            {
                return false;
            }

            _propertyBackingDictionary[propertyName] = newValue;

            return true;
        }

        /// <summary>
        /// Gets a DateTime value for the given property name with the DateTimeKind set to Utc.
        /// </summary>
        protected DateTime GetUtcDateTime([CallerMemberName] string propertyName = null)
        {
            if (propertyName == null)
            {
                throw new ArgumentNullException(nameof(propertyName));
            }

            // ReSharper disable once ExplicitCallerInfoArgument
            return DateTime.SpecifyKind(this.GetPropertyValue<DateTime>(propertyName), DateTimeKind.Utc);
        }

        /// <summary>
        /// Sets a DateTime value for the given property name with the DateTimeKind set to Utc.
        /// </summary>
        protected bool SetUtcDateTime(DateTime newValue, [CallerMemberName] string propertyName = null)
        {
            if (propertyName == null)
            {
                throw new ArgumentNullException(nameof(propertyName));
            }

            // ReSharper disable once ExplicitCallerInfoArgument
            return this.SetPropertyValue(DateTime.SpecifyKind(newValue, DateTimeKind.Utc), propertyName);
        }

        /// <summary>
        /// Gets a nullable DateTime value for the given property name with the DateTimeKind set to Utc.
        /// </summary>
        protected DateTime? GetNullableUtcDateTime([CallerMemberName] string propertyName = null)
        {
            if (propertyName == null)
            {
                throw new ArgumentNullException(nameof(propertyName));
            }

            // ReSharper disable once ExplicitCallerInfoArgument
            var date = this.GetPropertyValue<DateTime?>(propertyName);

            if (date == null)
            {
                return null;
            }

            return DateTime.SpecifyKind(date.Value, DateTimeKind.Utc);
        }

        /// <summary>
        /// Sets a nullable DateTime value for the given property name with the DateTimeKind set to Utc.
        /// </summary>
        protected bool SetNullableUtcDateTime(DateTime? newValue, [CallerMemberName] string propertyName = null)
        {
            if (propertyName == null)
            {
                throw new ArgumentNullException(nameof(propertyName));
            }

            if (newValue == null)
            {
                // ReSharper disable once ExpressionIsAlwaysNull
                // ReSharper disable once ExplicitCallerInfoArgument
                return this.SetPropertyValue(newValue, propertyName);
            }

            // ReSharper disable once ExplicitCallerInfoArgument
            return this.SetPropertyValue(DateTime.SpecifyKind(newValue.Value, DateTimeKind.Utc), propertyName);
        }
    }
}
