﻿namespace Slyno.WebApi.Data.Models
{
    public interface ISortable
    {
        int Sort { get; set; }
    }
}
