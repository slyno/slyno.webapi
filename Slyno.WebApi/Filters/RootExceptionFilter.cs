﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using Slyno.WebApi.Exceptions;

namespace Slyno.WebApi.Filters
{
    public class RootExceptionFilter : ExceptionFilterAttribute
    {
        private const string DefaultErrorMessage = "Bad request. No additional information available.";

        public bool ShowDetailedExceptions { get; set; }

        public override void OnException(HttpActionExecutedContext context)
        {
            var ex = context.Exception;
            var correlationId = context.Request.GetCorrelationId();

            if (ex is ValidationException)
            {
                context.Response = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = GetErrorContent(ex.Message, correlationId)
                };

                return;
            }

            var apiException = ex as ApiException;
            if (apiException != null)
            {
                context.Response = new HttpResponseMessage(apiException.HttpStatusCode)
                {
                    Content = GetErrorContent(apiException.Message, correlationId)
                };

                return;
            }

            var msg = DefaultErrorMessage;

            if (this.ShowDetailedExceptions)
            {
                msg = ex.ToString();
            }

            context.Response = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = GetErrorContent(msg, correlationId)
            };
        }

        private static JsonContent GetErrorContent(string message, Guid correlationId)
        {
            return new JsonContent(new ExceptionDtoOut(message, correlationId));
        }
    }
}
