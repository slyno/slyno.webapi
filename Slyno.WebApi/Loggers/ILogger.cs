﻿namespace Slyno.WebApi.Loggers
{
    public interface ILogger
    {
        void Log(string message);
    }
}
