﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Metadata;
using Newtonsoft.Json;

namespace Slyno.WebApi.Bindings
{
    public class CsvParameterBinding : HttpParameterBinding
    {
        public CsvParameterBinding(HttpParameterDescriptor descriptor) : base(descriptor)
        {
        }

        public override Task ExecuteBindingAsync(ModelMetadataProvider metadataProvider, HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            var paramName = this.Descriptor.ParameterName;

            var rawParamemterValue = actionContext.ControllerContext.RouteData.Values[paramName].ToString();

            var rawValues = rawParamemterValue.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            //To convert the raw value int a true JSON array we need to make sure everything is quoted.
            var jsonString = $"[\"{string.Join("\",\"", rawValues)}\"]";

            try
            {
                var obj = JsonConvert.DeserializeObject(jsonString, this.Descriptor.ParameterType);

                actionContext.ActionArguments[paramName] = obj;
            }
            catch
            {
                //There was an error casting, the jsonString must be invalid.
                //Don't set anything and the action will just receive null.
            }

            return Task.FromResult<object>(null);
        }
    }
}
