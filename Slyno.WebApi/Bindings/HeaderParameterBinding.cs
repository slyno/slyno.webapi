﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Metadata;
using Newtonsoft.Json;

namespace Slyno.WebApi.Bindings
{
    public class HeaderParameterBinding : HttpParameterBinding
    {
        private readonly string _headerName;

        public HeaderParameterBinding(HttpParameterDescriptor descriptor, string headerName) : base(descriptor)
        {
            _headerName = headerName;
        }

        public JsonSerializerSettings SerializerSettings { get; set; } = new JsonSerializerSettings();

        public override Task ExecuteBindingAsync(ModelMetadataProvider metadataProvider, HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            var request = actionContext.Request;

            IEnumerable<string> headerCollection;

            if (!request.Headers.TryGetValues(_headerName, out headerCollection))
            {
                return Task.FromResult<object>(null);
            }

            var headerItems = headerCollection.ToList();

            if (headerItems.Count != 1)
            {
                return Task.FromResult<object>(null);
            }

            var json = headerItems.FirstOrDefault();

            if (string.IsNullOrWhiteSpace(json))
            {
                return Task.FromResult<object>(null);
            }

            var dto = JsonConvert.DeserializeObject(json, this.Descriptor.ParameterType, this.SerializerSettings);

            if (dto == null)
            {
                return Task.FromResult<object>(null);
            }

            actionContext.ActionArguments[this.Descriptor.ParameterName] = dto;

            return Task.FromResult<object>(null);
        }
    }
}
