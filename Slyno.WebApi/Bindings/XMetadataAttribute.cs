﻿namespace Slyno.WebApi.Bindings
{
    public class XMetadataAttribute : FromHeaderAttribute
    {
        /// <summary>
        /// Binds a json string from the X-Metadata request header to the given type.
        /// This will use the default JsonSerializerSettings.
        /// </summary>
        public XMetadataAttribute() : base("X-Metadata")
        {
        }
    }
}
