﻿using System.Web.Http;
using System.Web.Http.Controllers;

namespace Slyno.WebApi.Bindings
{
    public class FromUriCsvAttribute : ParameterBindingAttribute
    {
        public override HttpParameterBinding GetBinding(HttpParameterDescriptor parameter)
        {
            return new CsvParameterBinding(parameter);
        }
    }
}
