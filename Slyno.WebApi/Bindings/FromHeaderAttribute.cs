﻿using System.Web.Http;
using System.Web.Http.Controllers;
using Newtonsoft.Json;

namespace Slyno.WebApi.Bindings
{
    public class FromHeaderAttribute : ParameterBindingAttribute
    {
        private readonly string _headerName;
        private readonly JsonSerializerSettings _serializerSettings;

        /// <summary>
        /// Binds a json string from the request header to the given type.
        /// </summary>
        /// <param name="headerName">Name of the Request Header.</param>
        public FromHeaderAttribute(string headerName)
        {
            _headerName = headerName;
            _serializerSettings = new JsonSerializerSettings();
        }

        /// <summary>
        /// Binds a json string from the request header to the given type.
        /// </summary>
        /// <param name="headerName">Name of the Request Header.</param>
        /// <param name="serializerSettings">Custom settins to use for deserializing.</param>
        public FromHeaderAttribute(string headerName, JsonSerializerSettings serializerSettings) : this(headerName)
        {
            _headerName = headerName;
            _serializerSettings = serializerSettings;
        }

        public override HttpParameterBinding GetBinding(HttpParameterDescriptor parameter)
        {
            return new HeaderParameterBinding(parameter, _headerName)
            {
                SerializerSettings = _serializerSettings
            };
        }
    }
}
