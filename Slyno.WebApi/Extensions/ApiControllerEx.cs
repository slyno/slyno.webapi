﻿using System.Web.Http;
using Slyno.WebApi.Auth;
using Slyno.WebApi.Results;

namespace Slyno.WebApi.Extensions
{
    public static class ApiControllerEx
    {
        public static IApiPrincipal GetApiPrincipal(this ApiController controller)
        {
            return controller.User as IApiPrincipal;
        }

        public static OkAcceptedResult OkAccepted(this ApiController controller)
        {
            return new OkAcceptedResult(controller);
        }

        public static OkNoContentResult OkNoContent(this ApiController controller)
        {
            return new OkNoContentResult(controller);
        }
    }
}
