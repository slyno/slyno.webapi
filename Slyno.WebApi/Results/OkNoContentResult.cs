﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace Slyno.WebApi.Results
{
    public class OkNoContentResult : OkResult
    {
        public OkNoContentResult(ApiController controller) : base(controller) { }

        public override async Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var response = await base.ExecuteAsync(cancellationToken);
            response.StatusCode = HttpStatusCode.NoContent;
            return response;
        }
    }
}
