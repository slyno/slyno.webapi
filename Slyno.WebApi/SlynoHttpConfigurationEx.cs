﻿using System.Diagnostics;
using System.Net.Http.Extensions.Compression.Core.Compressors;
using System.Web.Http;
using Microsoft.AspNet.WebApi.Extensions.Compression.Server;
using Newtonsoft.Json;
using Slyno.WebApi.Auth;
using Slyno.WebApi.Data.Attributes;
using Slyno.WebApi.Filters;

namespace Slyno.WebApi
{
    public static class SlynoHttpConfigurationEx
    {
        /// <summary>
        /// Enables the basic setup of Slyno including setting up the authentication and authorization filters, mapping routes, and gzip support.
        /// Authentication is done based on BearerToken.
        /// AllowAnonymous is required for every route, but can be overridden by using the [AllowAnnonymous] attribute.
        /// </summary>
        /// <param name="config"></param>
        public static void SlynoWebApi(this HttpConfiguration config)
        {
            config.MessageHandlers.Insert(0, new ServerCompressionHandler(new GZipCompressor(), new DeflateCompressor()));

            config.Filters.Add(new RootExceptionFilter { ShowDetailedExceptions = Debugger.IsAttached });

            config.Filters.Add(new ModelValidationFilterAttribute());

            config.Filters.Add(new AuthenticationAttribute());

            config.Filters.Add(new AuthorizationAttribute());

            config.MapHttpAttributeRoutes();

            var formatters = config.Formatters;

            //remove XML
            formatters.Remove(formatters.XmlFormatter);

            var serializerSettings = formatters.JsonFormatter.SerializerSettings;

            //ignores any circular references in DTO formatting
            serializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            //all datetimes should be formated as UTC dates.
            serializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
        }
    }
}
