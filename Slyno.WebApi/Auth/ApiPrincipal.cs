﻿using System;
using System.Security.Principal;

namespace Slyno.WebApi.Auth
{
    public class ApiPrincipal : IApiPrincipal
    {
        public ApiPrincipal(string accessToken, IUser user, IApplication application, DateTime expires)
        {
            this.AccessToken = accessToken;
            this.User = user;
            this.Application = application;
            this.Expires = expires;

            this.Identity = new ApiIdentity(user);
        }

        /// <summary>
        /// This method always returns false. Roles need to be determined by other logic, not this class.
        /// </summary>
        /// <param name="role"></param>
        public bool IsInRole(string role)
        {
            return false;
        }

        public IIdentity Identity { get; }

        public string AccessToken { get; }

        public IApplication Application { get; }

        public IUser User { get; }

        public DateTime Expires { get; }
    }
}
