﻿using System;
using System.Security.Principal;

namespace Slyno.WebApi.Auth
{
    public interface IApiPrincipal : IPrincipal
    {
        string AccessToken { get; }

        IApplication Application { get; }

        IUser User { get; }

        DateTime Expires { get; }
    }
}
