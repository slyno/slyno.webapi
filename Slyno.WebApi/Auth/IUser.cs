﻿using Slyno.WebApi.Data.Models;

namespace Slyno.WebApi.Auth
{
    public interface IUser : IModel, IUnique
    {
        string FirstName { get; set; }

        string LastName { get; set; }

        string Email { get; set; }
    }
}
