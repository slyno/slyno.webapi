﻿using System.Threading.Tasks;

namespace Slyno.WebApi.Auth
{
    public interface IApiPrincipalProvider
    {
        Task<IApiPrincipal> GetApiPrincipalAsync(string token);
    }
}
