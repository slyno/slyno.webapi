﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Slyno.WebApi.Exceptions;

namespace Slyno.WebApi.Auth
{
    public class SecureStringService : ISecureStringService
    {
        private readonly PasswordValidator _defaultPasswordValidator = new PasswordValidator
        {
            RequireDigit = true,
            RequireLowercase = true,
            RequireUppercase = true,
            RequiredLength = 6,
            RequireNonLetterOrDigit = false
        };

        public async Task ValidatePasswordAsync(string plainTextPassword)
        {
            await this.ValidatePasswordAsync(plainTextPassword, _defaultPasswordValidator);
        }

        public async Task ValidatePasswordAsync(string plainTextPassword, PasswordValidator passwordValidator)
        {
            var validationResult = await passwordValidator.ValidateAsync(plainTextPassword);

            if (!validationResult.Succeeded)
            {
                var errorMessage = string.Join(", ", validationResult.Errors);
                throw new PasswordValidationException(errorMessage);
            }
        }

        public string HashPassword(string plainTextPassword, string salt, int iterations = 10007)
        {
            if (string.IsNullOrEmpty(plainTextPassword))
            {
                throw new NullException("Password must not be null or empty.");
            }

            if (string.IsNullOrEmpty(salt))
            {
                throw new NullException("Salt must not be null or empty.");
            }

            var saltedPassword = string.Join("", salt, plainTextPassword, salt);

            var data = Encoding.UTF8.GetBytes(saltedPassword);

            using (var sha = new SHA512CryptoServiceProvider())
            {
                for (var i = 0; i < iterations; i++)
                {
                    data = sha.ComputeHash(data);
                }
            }

            return Convert.ToBase64String(data);
        }

        public string GenerateSalt(int minLength = 64, int maxLength = 1024)
        {
            if (minLength < 64)
            {
                throw new OutOfRangeException("Minimum length is 64.");
            }

            if (maxLength < minLength)
            {
                throw new OutOfRangeException("Maximum length must be greater than or equal to the minimum length.");
            }

            var random = new Random(Guid.NewGuid().GetHashCode());
            return this.GenerateRandomString(random.Next(minLength, maxLength));
        }

        public bool CheckPassword(string plainTextPassword, string salt, string hashedPasswordToCompare, int iterations)
        {
            if (string.IsNullOrEmpty(plainTextPassword) || string.IsNullOrEmpty(salt))
            {
                return false;
            }

            try
            {
                var hashedPassword = this.HashPassword(plainTextPassword, salt, iterations);

                return string.Equals(hashedPassword, hashedPasswordToCompare);
            }
            catch
            {
                return false;
            }
        }

        public string GenerateRandomString(int length = 64)
        {
            if (length < 1)
            {
                throw new OutOfRangeException("Length must be one or more.");
            }

            var sb = new StringBuilder();

            //GetRandomFileName uses RNGCryptoServiceProvider behind the scenes and returns an 11 character length string
            while (sb.Length < length)
            {
                sb.Append(Path.GetRandomFileName().Replace(".", ""));
            }

            //Let's make sure the final lenght is what was asked for.
            var properLengthString = sb.ToString().Substring(0, length);

            //GetRandomFileName only uses lowercase letters. Let's go through each character and randomly make it uppercase.
            var random = new Random(Guid.NewGuid().GetHashCode());

            var casedStringBuilder = new StringBuilder();
            foreach (var c in properLengthString.ToCharArray())
            {
                var shouldUppercase = random.Next() % 2 == 0;
                if (shouldUppercase)
                {
                    casedStringBuilder.Append(c.ToString().ToUpper());
                }
                else
                {
                    casedStringBuilder.Append(c);
                }
            }

            return casedStringBuilder.ToString();
        }
    }
}
