using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace Slyno.WebApi.Auth
{
    public interface ISecureStringService
    {
        Task ValidatePasswordAsync(string plainTextPassword);

        Task ValidatePasswordAsync(string plainTextPassword, PasswordValidator passwordValidator);

        string HashPassword(string plainTextPassword, string salt, int iterations = 10007);

        string GenerateSalt(int minLength = 64, int maxLength = 1024);

        bool CheckPassword(string plainTextPassword, string salt, string hashedPasswordToCompare, int iterations = 10007);

        string GenerateRandomString(int length = 64);
    }
}
