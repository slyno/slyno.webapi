﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using System.Web.Http.Results;
using Slyno.WebApi.Exceptions;

namespace Slyno.WebApi.Auth
{
    /// <summary>
    /// Used to force authentication by using a bearer token either as an Authorization header or a query string.
    /// This will call an IApiPrincipalProvider to do the access token verification.
    /// </summary>
    public class AuthenticationAttribute : FilterAttribute, IAuthenticationFilter
    {
        private const string Bearer = nameof(Bearer);
        private const string AccessToken = nameof(AccessToken);

        private readonly IEnumerable<AuthenticationHeaderValue> _challenges;

        public AuthenticationAttribute()
        {
            _challenges = new List<AuthenticationHeaderValue>
            {
                new AuthenticationHeaderValue(Bearer)
            };
        }

        public async Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            var token = GetAccessToken(context.Request);

            //no credentials, do nothing
            if (string.IsNullOrWhiteSpace(token))
            {
                return;
            }

            //hate service locator here, but you can't inject into a filter.
            var apiPrincipalProvider = context.Request.GetDependencyScope()?.GetService(typeof(IApiPrincipalProvider)) as IApiPrincipalProvider;
            if (apiPrincipalProvider == null)
            {
                throw new ServiceUnavailableException("Unable to load ApiPrincipalProvider.");
            }

            var apiPrincipal = await apiPrincipalProvider.GetApiPrincipalAsync(token);
            context.Principal = apiPrincipal;
        }

        public async Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            var response = await context.Result.ExecuteAsync(cancellationToken);
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                context.Result = new UnauthorizedResult(_challenges, context.Request);
            }
        }

        private static string GetAccessToken(HttpRequestMessage request)
        {
            var authorizationHeader = request.Headers.Authorization;

            if (authorizationHeader != null && authorizationHeader.Scheme == Bearer)
            {
                return authorizationHeader.Parameter;
            }

            var query = request.GetQueryNameValuePairs()
                .Where(x => string.Equals(x.Key, AccessToken, StringComparison.OrdinalIgnoreCase))
                .ToList();

            return query.Any() ? query.First().Value : null;
        }
    }
}
