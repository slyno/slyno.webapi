﻿using System.Security.Principal;

namespace Slyno.WebApi.Auth
{
    public class ApiIdentity : IIdentity
    {
        public ApiIdentity(IUser user)
        {
            this.Name = $"{user.FirstName} {user.LastName}".Trim();
        }

        public string Name { get; }

        public string AuthenticationType { get; } = "Bearer";

        public bool IsAuthenticated { get; } = true;
    }
}
