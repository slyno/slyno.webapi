﻿using Slyno.WebApi.Data.Models;

namespace Slyno.WebApi.Auth
{
    public interface IApplication : IModel, IUnique
    {
        string Name { get; set; }

        string Version { get; set; }
    }
}
