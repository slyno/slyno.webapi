﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.Results;

namespace Slyno.WebApi.Auth
{
    /// <summary>
    /// Verifies authorization by making sure the IApiPrincipal is set on the request context.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class AuthorizationAttribute : AuthorizationFilterAttribute
    {
        private const string Bearer = nameof(Bearer);

        public override async Task OnAuthorizationAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            if (actionContext == null)
            {
                throw new ArgumentNullException(nameof(actionContext));
            }

            if (SkipAuthorization(actionContext))
            {
                return;
            }

            var apiPrincipal = actionContext.RequestContext.Principal as IApiPrincipal;
            if (apiPrincipal == null)
            {
                var challenges = new List<AuthenticationHeaderValue>
                {
                    new AuthenticationHeaderValue(Bearer)
                };

                var result = new UnauthorizedResult(challenges, actionContext.Request);
                actionContext.Response = await result.ExecuteAsync(CancellationToken.None);
            }
        }

        private static bool SkipAuthorization(HttpActionContext actionContext)
        {
            return actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any()
                   || actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any();
        }
    }
}
